import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: '/carrossel',
    name: 'Carrossel',
    component: () => import(/* webpackChunkName: "home" */ '../components/Carrossel.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
